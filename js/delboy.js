var isCreating = 0;
$(document).ready(function(){
  $('.modal').modal({
    dismissible: false, // Modal can be dismissed by clicking outside of the modal
    opacity: .5,
  })
  if(localStorage.getItem('sessionInfo')){
    var sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
    var myObj = {};
    myObj['userId'] = sessionInfo.userId;
    myObj['userType'] = sessionInfo.userType;
    myObj['typeId'] = sessionInfo.typeId;
    var jsonObj = JSON.stringify(myObj);
    var head = sessionInfo.basicAuthenticate;
    $.ajax({
      url : beUrl()+'/getdmlist',
      type: 'POST',
      dataType:'json',
      processData:false,
      contentType: 'application/json',
      headers : {"basicauthenticate":""+head},
      data:jsonObj,
      success: function(response) {
        var dmInfo = JSON.stringify(response);
        var dmData = JSON.parse(dmInfo);
        var print = "";
        if(dmData.totalCount>0){
          var data = dmData.dmList;
          $.each(data, function(i,v){
            print = print+"<div class='card darken-1 col s12 l3 z-depth-1' style='margin:1em;'><div class='card-content black-text'><span class='card-title'>Name: "+v.dmName+"</span><p class='headas' style='font-size:1em;'>Phone Number: "+v.dmPhone+"</p></div></div>";
          })
        }
        else{
          print = "<div class=row><div class=col s12 m6><div class=card blue-grey darken-1><div class=card-content white-text><span class=card-title>Card Title</span><p>Kindly add some delivery boys before proceeding.</p></div></div></div></div>";
        }
        $('#deliveryBoyList').append(print);
      }
    });
  } $("#signUp1").submit(function(event){
          event.preventDefault();
          $("#signup1").click();
      });
  $("#signup1").click(function(){
    var userNameId = document.getElementById("signupUsername")
    var userNameClass = userNameId.className;
    var userPassId = document.getElementById("signuppassword");
    var userPassClass = userPassId.className;
    if((userNameClass=="validate valid")&&(userPassClass=="validate valid")){
      var signUpObj = {}
      signUpObj['userNameLogin']=userNameId.value;
      signUpObj['password']=userPassId.value;
      var signUpJson = JSON.stringify(signUpObj);
      sessionStorage.setItem('signUpJson',signUpJson)
      if(sessionStorage.getItem('signUpJson')){
        Materialize.toast('Almost there', 4000);
        $('#signUpOnly1').modal('close');
        $('#signUpOnly2').modal('open');
      }
    }
    else {
      Materialize.toast('See the error message', 4000);
    }
  });
    $("#signUp2").submit(function(event){
          event.preventDefault();
          $("#signup2").click();
      });
  $("#signup2").click(function(){
    event.preventDefault();
    var custNameId = document.getElementById('signupCustname');
    var custEmailId = document.getElementById('userEmail');
    var custPhoneId = document.getElementById('userPhone');
    var custNameClass = custNameId.className;
    var custEmailClass = custEmailId.className;
    var custPhoneClass = custPhoneId.className;
    if((custNameClass=="validate valid")&&(custEmailClass=="validate valid")&&(custPhoneClass=="validate valid"))
    {
      var loginObject = sessionStorage.getItem('signUpJson');
      var loginPars = JSON.parse(loginObject);
      sessionStorage.removeItem('signUpJson');
      var signUpObject = {};
      signUpObject['userType']="CUST";
      signUpObject['userNameLogin']=loginPars.userNameLogin;
      signUpObject['password']=loginPars.password;
      signUpObject['phoneNumber']=custPhoneId.value;
      signUpObject['custName']=custNameId.value;
      signUpObject['emailId']=custEmailId.value;
      var jsonSignUpJson = JSON.stringify(signUpObject);
      sessionStorage.setItem('signUpJson',jsonSignUpJson);
      var otp = {};
      otp['phoneNumber']=custPhoneId.value;
      otp['userType']="DM";
      otp['userNeed']="CREATE";
      var jsonOtp =JSON.stringify(otp);
      $.ajax({
        url : beUrl()+'/generateotp',
        type: 'POST',
        dataType:'json',
        processData:false,
        contentType: 'application/json',
        data:jsonOtp,
        success: function(response) {
          Materialize.toast('Please see your phone for OTP', 4000);
          $('#signUpOnly2').modal('close');
          $('#signUpOnly3').modal('open');
        },
        error: function (response) {
          var responseJson = JSON.stringify(response);
          var responseJsonR = JSON.parse(responseJson);
          var statusCode = responseJsonR.responseJSON;
          var data = JSON.stringify(statusCode);
          var xData = JSON.parse(data);
          var message = xData.Data;
          Materialize.toast(message, 4000);
        }
      });
    }
    else {
      Materialize.toast('See the error message', 4000);
    }
  });
    $("#signUp3").submit(function(event){
          event.preventDefault();
          $("#signup3").click();
      });
  $("#signup3").click(function(){
    event.preventDefault();
    if(isCreating==0){
          isCreating = 1;
          var otpId = document.getElementById('custOTP');
          var otpclass = otpId.className;
          if(otpclass=="validate valid"){
            var signUpObjectFinal = sessionStorage.getItem('signUpJson');
            var jsonParser = JSON.parse(signUpObjectFinal);
            var finalObject = {};
            finalObject['otp']=otpId.value;
            finalObject['userNameLogin']=jsonParser.userNameLogin;
            finalObject['password']=jsonParser.password;
            finalObject['phoneNumber']=jsonParser.phoneNumber;
            finalObject['dmName']=jsonParser.custName;
            finalObject['emailId']=jsonParser.emailId;
            finalObject['userType']="DM";
            finalObject['typeId'] = sessionInfo.typeId;
            var finalJsonObject = JSON.stringify(finalObject);
            var head = sessionInfo.basicAuthenticate;
            $.ajax({
              url : beUrl()+'/createuser',
              type: 'POST',
              dataType:'json',
              processData:false,
              contentType: 'application/json',
              headers : {"basicauthenticate":""+head},
              data:finalJsonObject,
              success: function(response) {
                Materialize.toast('Please wait for magic', 4000);
                var sessionInfo = JSON.stringify(response);
                setTimeout(function(){
                  location.reload();
                }, 1000);
              },
              error: function (response) {mysq
                var responseJson = JSON.stringify(response);
                var responseJsonR = JSON.parse(responseJson);
                var statusCode = responseJsonR.responseJSON;
                var data = JSON.stringify(statusCode);
                var xData = JSON.parse(data);
                var message = xData.Data;
                Materialize.toast(message, 4000);
                isCreating = 0;
              }
            });
          }
          else {
            Materialize.toast('Please pass OTP', 4000);
          }
          console.log(otpclass);
    }
    else{
      Materialize.toast('JINNI is adding magic please remain patient', 4000);
    }
  })
});

function userAvailability() {
  var userData = document.getElementById('signupUsername').value;
  myObj = {};
  myObj['userName'] = userData;
  myObj['mode']="client";
  var jsonData = JSON.stringify(myObj);
  $.ajax({
    url : beUrl()+'/validateuser',
    type: 'POST',
    dataType:'json',
    processData:false,
    contentType: 'application/json',
    data:jsonData,
    error: function(response) {
      $("#signupUsername").next("label").attr('data-error','The userName is already taken');
      $("#signupUsername").removeClass("valid");
      $("#signupUsername").addClass("invalid");
    },
    success: function (response) {
      $("#signupUsername").next("label").attr('data-success','The userName is available');
      $("#signupUsername").removeClass("invalid");
      $("#signupUsername").addClass("valid");
    }
  });
}

function passwordValidity() {
  var validatePass = document.getElementById('signuppassword');
  var length = validatePass.value.length;
  if(length<8){
    $("#signuppassword").next("label").attr('data-error','Too small password');
    $("#signuppassword").removeClass("valid");
    $("#signuppassword").addClass("invalid");
  }
  else if (length>24) {
    $("#signuppassword").next("label").attr('data-error','Too big password');
    $("#signuppassword").removeClass("valid");
    $("#signuppassword").addClass("invalid");
  }
  else {
    $("#signuppassword").next("label").attr('data-success','The password is acceptable');
    $("#signuppassword").removeClass("invalid");
    $("#signuppassword").addClass("valid");
  }
}
function phoneAvailablity() {
  var validatePhone = document.getElementById('userPhone');
  var length = validatePhone.value.length;
  if(length<10){
    $("#userPhone").next("label").attr('data-error','Too small phone number');
    $("#userPhone").removeClass("valid");
    $("#userPhone").addClass("invalid");
  }
  else if (length>10) {
    $("#userPhone").next("label").attr('data-error','Too big phone number');
    $("#userPhone").removeClass("valid");
    $("#userPhone").addClass("invalid");
  }
  else {
    var phoneNumber = validatePhone.value;
    var phoneObj = {};
    phoneObj['phoneNumber']=phoneNumber;
    localStorage.setItem('phoneNumber',phoneNumber);
    phoneObj['userType']="DM";
    var jsonPhone = JSON.stringify(phoneObj);
    $.ajax({
      url : beUrl()+'/validphone',
      type: 'POST',
      dataType:'json',
      processData:false,
      contentType: 'application/json',
      data:jsonPhone,
      error: function(response) {
        $("#userPhone").next("label").attr('data-error','The phoneNumber is already taken');
        $("#userPhone").removeClass("valid");
        $("#userPhone").addClass("invalid");
      },
      success: function (response) {
        $("#userPhone").next("label").attr('data-success','The phoneNumber is available');
        $("#userPhone").removeClass("invalid");
        $("#userPhone").addClass("valid");
      }
    });
  }
}

function addDeliveryMedium() {
  var sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
  var requestObject = {};
  requestObject['typeId']=sessionInfo.typeId;
  requestObject['userType']=sessionInfo.userType;
  requestObject['userId'] = sessionInfo.userId;
  requestObject['propertyName']="maxDelboyCount";
  var finalJsonObject = JSON.stringify(requestObject);
  var head = sessionInfo.basicAuthenticate;
  $.ajax({
    url : beUrl()+'/validateshopprop',
    type: 'POST',
    dataType:'json',
    processData:false,
    contentType: 'application/json',
    headers : {"basicauthenticate":""+head},
    data:finalJsonObject,
    success: function(response) {
      $('#signUpOnly1').modal('open');
    },
    error: function (response) {
      Materialize.toast('Max delivery boy already present', 4000);
    }
  });
}

function resendotp()
{
  console.log('OTP resent');
  var phoneNumber=JSON.parse(sessionStorage.getItem('signUpJson')).phoneNumber;
  console.log(phoneNumber);
  var otp = {};
  otp['phoneNumber']=phoneNumber;
  otp['userType']="CUST";
  otp['userNeed']="SIGNUP";
  var jsonOtp =JSON.stringify(otp);
  $.ajax({
    url : beUrl()+'/generateotp',
    type: 'POST',
    dataType:'json',
    processData:false,
    contentType: 'application/json',
    data:jsonOtp,
    success: function(response) {
      Materialize.toast('OTP has been resent', 4000);
      $('#signUpOnly3').modal('open');
    },
    error: function (response) {
      var responseJson = JSON.stringify(response);
      var responseJsonR = JSON.parse(responseJson);
      var statusCode = responseJsonR.responseJSON;
      var data = JSON.stringify(statusCode);
      var xData = JSON.parse(data);
      var message = xData.Data;
      Materialize.toast(message, 4000);
    }
  });
}
