$(document).ready(function(){
    $('.modal').modal();
    $('ul.tabs').tabs({

    });
    localStorage.removeItem('delOrderId');
    localStorage.removeItem('selectedDeliveryMedium');
    var sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
    var myObj = {};
    myObj['userId'] = sessionInfo.userId;
    myObj['userType'] = sessionInfo.userType;
    myObj['typeId'] = sessionInfo.typeId;
    myObj['viewType'] = "NEW"
    var jsonObj = JSON.stringify(myObj);
    var head = sessionInfo.basicAuthenticate;
    $.ajax({ 
      url : beUrl()+'/orderlist',
      type: 'POST',
      dataType:'json',
      processData:false,
      contentType: 'application/json',
      headers : {"basicauthenticate":""+head},
      data:jsonObj,
      success: function(response) {
        var jsonData = JSON.stringify(response);
        var dataUse = JSON.parse(jsonData);
        var data = dataUse.Data;
        var print = ""
        var totalCount = dataUse.totalCount;
        if(totalCount>0){
          $.each(data, function(i,v){
            print = print+"<tr><td><a href="+feUrl()+"/orderdetails.html?orderId="+v.orderId+" class='btn'>"+v.custName+"</a></td><td>"+v.amount+"</td><td>"+v.orderStatus+"</td> <td>"+v.paymentMethod+"</td></tr>";
          })
        }
        else{
          print = "<div class=card blue-grey darken-1><div class=card-content white-text><span class='card-title as_head'></span><p>Sorry currenty no orders in this section.</p></div></div>";
        }
        $('#newOrders').append(print);
      }
    });
    myObj['viewType'] = "INPROGRESS"
    var jsonObj = JSON.stringify(myObj);
    $.ajax({
      url : beUrl()+'/orderlist',
      type: 'POST',
      dataType:'json',
      processData:false,
      contentType: 'application/json',
      headers : {"basicauthenticate":""+head},
      data:jsonObj,
      success: function(response) {
        var jsonData = JSON.stringify(response);
        var dataUse = JSON.parse(jsonData);
        var data = dataUse.Data;
        var print = ""
        var totalCount = dataUse.totalCount;
        if(totalCount>0){
          $.each(data, function(i,v){
            if((v.orderStatus=="ACCEPTED"||v.orderStatus=="Accepted"||v.orderStatus=="WORKING"||v.orderStatus=="Working")&(v.itemsLeft>0)){
              print = print+"<tr><td><a href="+feUrl()+"/orderdetails.html?orderId="+v.orderId+" class='btn'>"+v.custName+"</td><td>"+v.amount+"</td><td>"+v.orderStatus+"</td><td>"+v.paymentMethod+"</td><td>"+v.itemsLeft+"</td><td><a href=javascript:assignOrderDm("+v.orderId+") class='btn btn-outline'>Assign</a></td></tr>";
            }
            else{
              print = print+"<tr><td><a href="+feUrl()+"/orderdetails.html?orderId="+v.orderId+" class='btn'>"+v.custName+"</td><td>"+v.amount+"</td><td>"+v.orderStatus+"</td><td>"+v.paymentMethod+"</td><td>"+v.itemsLeft+"</td><td>Already Assigned</td></tr>";
            }
          });
        }
        else{
          print = "<div class=row><div class=col s12 m6><div class=card blue-grey darken-1><div class=card-content white-text><span class='card-title as_head'></span><p>Sorry currenty no orders in this section.</p></div></div></div></div>";
        }
        $('#inProgOrders').append(print);
      }
    });
    myObj['viewType'] = "COMPLETED";
    var jsonObj = JSON.stringify(myObj);
    $.ajax({
      url : beUrl()+'/orderlist',
      type: 'POST',
      dataType:'json',
      processData:false,
      contentType: 'application/json',
      headers : {"basicauthenticate":""+head},
      data:jsonObj,
      success: function(response) {
        var jsonData = JSON.stringify(response);
        var dataUse = JSON.parse(jsonData);
        var data = dataUse.Data;
        var print = ""
        var totalCount = dataUse.totalCount;
        if(totalCount>0){
          $.each(data, function(i,v){
            print = print+"<tr><td><a href="+feUrl()+"/orderdetails.html?orderId="+v.orderId+" class='btn'>"+v.custName+"</a></td><td>"+v.amount+"</td><td>"+v.orderStatus+"</td> <td>"+v.paymentMethod+"</td></tr>";
          })
        }
        else{
          print = "<div class=row><div class=col s12 m6><div class=card blue-grey darken-1><div class=card-content white-text><span class='card-title as_head'></span><p>Sorry currenty no orders in this section.</p></div></div></div></div>";
        }
        $('#compOrders').append(print);
      }
    });
  });
function acceptOrder(orderId) {
  if(localStorage.getItem('sessionInfo')){
    var sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
    var myObj = {};
    myObj['userId'] = sessionInfo.userId;
    myObj['typeId'] = sessionInfo.typeId;
    myObj['userType'] = sessionInfo.userType;
    myObj['orderId'] = orderId;
    myObj['orderStatus'] = "ACCEPTED";
    var jsonObj = JSON.stringify(myObj);
    var head = sessionInfo.basicAuthenticate;
    $.ajax({
      url : beUrl()+'/updateorderstatus',
      type: 'POST',
      dataType:'json',
      processData:false,
      contentType: 'application/json',
      headers : {"basicauthenticate":""+head},
      data:jsonObj,
      success: function(response) {
        Materialize.toast('Order ACCEPTED Please wait', 4000)
        setTimeout(function(){
           location.reload();
        }, 500);
      }
    });
  }
  console.log(orderId);
}

function assignOrderDm(orderId) {
  console.log(orderId);
  localStorage.setItem('delOrderId',orderId);
  var sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
  var myObj = {};
  myObj['userId'] = sessionInfo.userId;
  myObj['typeId'] = sessionInfo.typeId;
  myObj['userType'] = sessionInfo.userType;
  var head = sessionInfo.basicAuthenticate;
  var jsonObj = JSON.stringify(myObj);
  myObj['orderId'] = orderId;
  var useJson = JSON.stringify(myObj);
  $.ajax({
    url : beUrl()+'/vieworderformark',
    type: 'POST',
    dataType:'json',
    processData:false,
    contentType: 'application/json',
    headers : {"basicauthenticate":""+head},
    data:useJson,
    success: function(response) {
      if((JSON.parse(JSON.stringify(response))).totalCount>0){
        localStorage.setItem('itemListForMark',JSON.stringify(response));
        $.ajax({
          url : beUrl()+'/getdmlist',
          type: 'POST',
          dataType:'json',
          processData:false,
          contentType: 'application/json',
          headers : {"basicauthenticate":""+head},
          data:jsonObj,
          success: function(response) {
            var dmInfo = JSON.stringify(response);
            var dmData = JSON.parse(dmInfo);
            var print = "";
            if(dmData.totalCount>0){
              var data = dmData.dmList;
              $.each(data, function(i,v){
                if(!document.getElementById(v.dmId)){
                    print = print+"<p><input name=group1 class=with-gap type=radio id="+v.dmId+" onclick=handleClick("+v.dmId+") /><label for="+v.dmId+"><h5 class='subhead'>Delivery Boy: "+v.dmName+"</h5></label><p class='content'>Phone Number "+v.dmPhone+"</p></p><hr>";
                }
              })
            }
            else{
              print = "<div class=row><div class=col s12 m6><div class=card blue-grey darken-1><div class=card-content white-text><span class='card-title as_head'></span><p>Kindly add some delivery boys before proceeding.</p></div></div></div></div>";
            }
            $('#deliveryBoyList').append(print);
            $('#dmAssignment').modal('open');
          }
        });
      }
      else{
          window.alert("Nothing left to assign");
      }
  }
  });
}

function handleClick(dmId) {
  localStorage.setItem('selectedDeliveryMedium',dmId);
}

function proceedWithAssignment() {
  if(localStorage.getItem('selectedDeliveryMedium')){
    $('#dmAssignment').modal('close');
    var sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
    var myObj = {};
    myObj['userId'] = sessionInfo.userId;
    myObj['typeId'] = sessionInfo.typeId;
    myObj['userType'] = sessionInfo.userType;
    myObj['dmId'] = parseInt(localStorage.getItem('selectedDeliveryMedium'));
    myObj['items'] = JSON.parse(localStorage.getItem('itemListForMark')).data;
    myObj['orderId'] = parseInt(localStorage.getItem('delOrderId'));
    var finalJsonObject = JSON.stringify(myObj);
    var head = sessionInfo.basicAuthenticate;
    $.ajax({
      url : beUrl()+'/assigndm',
      type: 'POST',
      dataType:'json',
      processData:false,
      contentType: 'application/json',
      headers : {"basicauthenticate":""+head},
      data:finalJsonObject,
      success: function(response) {
        Materialize.toast('Order ASSIGNED Please wait', 4000)
        setTimeout(function(){
           location.reload();
        }, 100);
      }
    });
    console.log(myObj);
  }
  else{
    Materialize.toast('Select a delivery boy first', 4000)
    setTimeout(function(){
       $('#dmAssignment').modal('open');
    }, 500);
  }
}
