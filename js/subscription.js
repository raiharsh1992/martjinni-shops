$(document).ready(function(){
  $('.modal').modal({
    dismissible: true, // Modal can be dismissed by clicking outside of the modal
    opacity: .5,
  })
  $('.timepicker').pickatime({
   default: 'now', // Set default time: 'now', '1:30AM', '16:30'
   fromnow: 0,       // set default time to * milliseconds from now (using with default = 'now')
   twelvehour: false, // Use AM/PM or 24-hour format
   donetext: 'OK', // text for done-button
   cleartext: 'Clear', // text for clear-button
   canceltext: 'Cancel', // Text for cancel-button
   autoclose: true, // automatic close timepicker
   ampmclickable: true, // make AM PM clickable
   aftershow: function(){} //Function for after opening timepicker
 });
  $('#isActiveSubs').material_select();
  if(localStorage.getItem("sessionInfo")){
    var sessionInfo = JSON.parse(localStorage.getItem("sessionInfo"));
    var myObj = {};
    myObj["shopId"] = sessionInfo.typeId;
    var jsonObj = JSON.stringify(myObj);
    $.ajax({
      url : beUrl()+'/viewshoptime',
      type: 'POST',
      dataType:'json',
      processData:false,
      contentType: 'application/json',
      data:jsonObj,
      success: function(response) {
        var useData = JSON.parse(JSON.stringify(response));
        if(useData.totalCount>0){
          localStorage.setItem('existsGroupTime','YES');
          $.each(useData.Data, function(i, v){
              var myObjUser1 = {};
              if(v.isActive==1){
                  var print = "<tr><td>"+v.name+"</td><td>"+v.desc+"</td><td>"+v.time+"</td><td>YES</td></tr>";
              }
              else{
                var print = "<tr><td>"+v.name+"</td><td>"+v.desc+"</td><td>"+v.time+"</td><td>YES</td></tr>";
              }
              $('#groupInfoTable').append(print);
              myObjUser1['name']=v.name;
              myObjUser1['desc']=v.desc;
              myObjUser1['isActive']=v.isActive;
              myObjUser1['time']=v.time;
              var use= JSON.stringify(myObjUser1);
              var timeName = "timeGroup"+v.timeId;
              sessionStorage.setItem(timeName,use);
          });
        }
      }
    });
    $.ajax({
      url : beUrl()+'/shopsubsinfo',
      type: 'POST',
      dataType:'json',
      processData:false,
      contentType: 'application/json',
      data:jsonObj,
      success: function(response) {
        var data = JSON.parse(JSON.stringify(response)).data;
        $.each(data, function(i,v){
          var subsName = "subs"+v.subscriptionId;
          var myObjStore = {};
          myObjStore['noOfDays']=v.noOfDays;
          myObjStore['suscriptionPrice']=v.suscriptionPrice;
          myObjStore['isActive']=v.isActive;
          var jsonUseObj = JSON.stringify(myObjStore);
          localStorage.setItem(subsName,jsonUseObj);
            var val=v.isActive;
            if(val==0){
                val ="Not Active";
            }
            else{
                val="Active";
            }
          var xPrint = "<tr><td>"+v.noOfDays+"</td><td>"+v.suscriptionPrice+"</td><td>"+val+"</td><td><a class='btn-small waves-effect waves-light btn-outline' href=javascript:updateSubsInfo("+v.subscriptionId+")><i class='material-icons' style='color:#ffa751;'>edit</i></a></td></tr>";
          $('#subsInfo').append(xPrint);
        });
      }
    });
      $("#updatingInfoSubs").submit(function(event){
          event.preventDefault();
          $("#updaterSubInfo").click();
      });
  $("#updaterSubInfo").click(function(){
    var myObj1 = {};
    myObj1["noOfDays"]=document.getElementById('updateSubsNoDay').value;
    myObj1["subValue"]=document.getElementById('updateSubsPrice').value;
    myObj1["isActiveFl"]=document.getElementById('isActiveSubs').value;
    myObj1["userId"]=sessionInfo.userId;
    myObj1["typeId"]=sessionInfo.typeId;
    myObj1["userType"]=sessionInfo.userType;
    var head = sessionInfo.basicAuthenticate;
    var jsonData = JSON.stringify(myObj1);
    $.ajax({
      url : beUrl()+'/updatesubsinfo',
      type: 'POST',
      dataType:'json',
      processData:false,
      contentType: 'application/json',
      headers : {"basicauthenticate":""+head},
      data:jsonData,
      success: function(response) {
        Materialize.toast('Subscription updated', 4000);
        setTimeout(function(){
          location.reload();
        }, 1000);
      },
      error: function (response) {
        var responseJson = JSON.stringify(response);
        var responseJsonR = JSON.parse(responseJson);
        var statusCode = responseJsonR.responseJSON;
        var data = JSON.stringify(statusCode);
        var xData = JSON.parse(data);
        var message = xData.Data;
        Materialize.toast(message, 4000);
      }
    });
  });$("#cratingInfoSubs").submit(function(event){
      event.preventDefault();
      $("#creatorSubInfo").click();
  })
  $("#creatorSubInfoGroup").click(function(){
    var xMyObj = {};
    xMyObj['name']=document.getElementById('subsGroupCreateName').value;
    xMyObj['desc']=document.getElementById('subsGroupDescCreate').value;
    xMyObj['time']=document.getElementById('selectingSubscriptionTime').value;
    xMyObj["userId"]=sessionInfo.userId;
    xMyObj["typeId"]=sessionInfo.typeId;
    xMyObj["userType"]=sessionInfo.userType;
    var head = sessionInfo.basicAuthenticate;
    var jsonData = JSON.stringify(xMyObj);
    $.ajax({
      url : beUrl()+'/insertshoptime',
      type: 'POST',
      dataType:'json',
      processData:false,
      contentType: 'application/json',
      headers : {"basicauthenticate":""+head},
      data:jsonData,
      success: function(response) {
        Materialize.toast('A wish created', 4000);
        setTimeout(function(){
          location.reload();
        }, 1000);
      },
      error: function (response) {
        var responseJson = JSON.stringify(response);
        var responseJsonR = JSON.parse(responseJson);
        var statusCode = responseJsonR.responseJSON;
        var data = JSON.stringify(statusCode);
        var xData = JSON.parse(data);
        var message = xData.Data;
        Materialize.toast(message, 4000);
      }
    });

  });
  $("#creatorSubInfo").click(function(){
    var myObj1 = {};
    myObj1["noOfDays"]=document.getElementById('createSubsNoDay').value;
    myObj1["subValue"]=document.getElementById('createSubsPrice').value;
    myObj1["userId"]=sessionInfo.userId;
    myObj1["typeId"]=sessionInfo.typeId;
    myObj1["userType"]=sessionInfo.userType;
    var head = sessionInfo.basicAuthenticate;
    var jsonData = JSON.stringify(myObj1);
    $.ajax({
      url : beUrl()+'/insertsubsinfo',
      type: 'POST',
      dataType:'json',
      processData:false,
      contentType: 'application/json',
      headers : {"basicauthenticate":""+head},
      data:jsonData,
      success: function(response) {
        Materialize.toast('Subscription updated', 4000);
        setTimeout(function(){
          location.reload();
        }, 1000);
      },
      error: function (response) {
        var responseJson = JSON.stringify(response);
        var responseJsonR = JSON.parse(responseJson);
        var statusCode = responseJsonR.responseJSON;
        var data = JSON.stringify(statusCode);
        var xData = JSON.parse(data);
        var message = xData.Data;
        Materialize.toast(message, 4000);
      }
    });
  });
  }else{
    window.location.replace(feUrl()+"/index.html")
  }
});

function workWithSubsGroup(){
  if(localStorage.getItem('existsGroupTime')){
    $('#vieGroupInfo').modal('open');
  }
  else{
    $('#groupInfoSubs').modal('open');
  }
}

function openCreateGroup() {
  $('#vieGroupInfo').modal('close');
  $('#groupInfoSubs').modal('open');
}

function updateSubsInfo(subscriptionId) {
  var subsName = "subs"+subscriptionId;
  var subsInfo = JSON.parse(localStorage.getItem(subsName));
  document.getElementById('updateSubsNoDay').value = subsInfo.noOfDays;
  document.getElementById('updateSubsPrice').value = subsInfo.suscriptionPrice;
  document.getElementById('isActiveSubs').value = subsInfo.isActive;
  $('#updateSubsInfo').modal('open');
  $("#updateSubsNoDay").focus();
  $("#updateSubsPrice").focus();
  $("#isActiveSubs").focus();
}
