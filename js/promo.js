 $(document).ready(function() {
     $('select').material_select();
     $('.modal').modal({
         dismissible: true, // Modal can be dismissed by clicking outside of the modal
         opacity: .5,
     });
     if (localStorage.getItem('sessionInfo')) {
         var sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
         var myObj = {};
         myObj['userId'] = sessionInfo.userId;
         myObj['userType'] = sessionInfo.userType;
         myObj['typeId'] = sessionInfo.typeId;
         var jsonObj = JSON.stringify(myObj);
         var head = sessionInfo.basicAuthenticate;
         $.ajax({
             url: beUrl() + '/getpromolist',
             type: 'POST',
             dataType: 'json',
             processData: false,
             contentType: 'application/json',
             headers: {
                 "basicauthenticate": "" + head
             },
             data: jsonObj,
             success: function(response) {
                 var dmInfo = JSON.stringify(response);
                 var dmData = JSON.parse(dmInfo);
                 var print = "";
                 if (dmData.count > 0) {
                     var data = dmData.Data;
                     $.each(data, function(i, v) {
                         var multi = "False";
                         var DiscountA = 'Rs';
                         var ActiveStatus = "Deactivated";
                         var ChangeStatus = "Active";
                         var ChangeStatusb = "Activate";
                         if (v.isMulti == 1) {
                             multi = "True";
                         }
                         if (v.promoType == "P") {
                             DiscountA = "%";
                         }
                         if (v.isActive == 1) {
                             ActiveStatus = "Active";
                             ChangeStatus = "Deactivated";
                             ChangeStatusb = "Deactivate";
                         }
                         print = print + "<div class='card darken-1 col s12 l4 m4 xl4 z-depth-4'><div class='card-content black-text'><span class='card-title headas1'>Name: " + v.promoCode + "</span><p class='headas'>Created On: " + v.createdOn + "</p><p class='headas'>Multi Use: " + multi + "</p> <p class='headas'>Minimum Amount: " + v.minAmount + "</p><p class='headas'>Minimum Amount: " + v.disAmount + " " + DiscountA + "</p> <p class='headas'>Status: " + ActiveStatus + " <span style='float:right'><a class='waves-effect waves-light btn' href='#' onclick='togglePromo("+v.promoId+","+v.isActive+");return false;' >" + ChangeStatusb + " </a></span></p></div></div>";
                     })
                 } else {
                     print = "<div class=row><div class=col s12 m6><div class=card blue-grey darken-1><div class=card-content white-text><span class=card-title>Card Title</span><p>No Promocodes created Yet.</p></div></div></div></div>";
                 }
                 $('#dpromoCodeList').append(print);
             }
         });


 $("#createPromoForm").submit(function(event) {
     event.preventDefault();
     $("#createPromo").click();
 });
 $("#createPromo").click(function() {
     if(localStorage.getItem("sessionInfo"))
         {
             console.log("Function called");
              var sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
             var myObj = {};
             var typeId = sessionInfo.typeId;
             var userId = sessionInfo.userId;
             var userType= sessionInfo.userType;


             var promoCode =  document.getElementById('promoCode').value;
             var promoType = document.getElementById('promoType').value;
             var promoAmount =  document.getElementById('promoAmount').value;
             var isMulti = document.getElementById('isMulti').value;
             var minValue = document.getElementById('minValue').value;
             var head = sessionInfo.basicAuthenticate;
             myObj['typeId'] = typeId;
             myObj['userId'] = userId;
             myObj['userType'] = userType;
             myObj['minValue'] = parseInt(minValue) ;
             myObj['isMulti'] = isMulti;
             myObj['promoType'] = promoType;
             myObj['promoAmount'] = parseInt(promoAmount);
             myObj['promoCode'] = promoCode;
             var jsonObj = JSON.stringify(myObj);
            $.ajax({
                url : beUrl()+'/insertpromo',
                type: 'POST',
                dataType:'json',
                processData:false,
                contentType: 'application/json',
                headers : {"basicauthenticate":""+head},
                data:jsonObj,
                success: function(response) {
                    Materialize.toast('Promo Created', 4000);
                    setTimeout(function(){
                        location.reload();
                    }, 1000);
                },
                error: function (response) {
                    var responseJson = JSON.stringify(response);
                    var responseJsonR = JSON.parse(responseJson);
                    var statusCode = responseJsonR.responseJSON;
                    var data = JSON.stringify(statusCode);
                    var xData = JSON.parse(data);
                    var message = xData.Data;
                    Materialize.toast(message, 4000);
        }
            });
         }
     else{
         window.location.replace("index.html");
     }
 }); }

 });
 function addPromoCode() {
   var sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
   var requestObject = {};
   requestObject['typeId']=sessionInfo.typeId;
   requestObject['userType']=sessionInfo.userType;
   requestObject['userId'] = sessionInfo.userId;
   requestObject['propertyName']="maxDelboyCount";
   var finalJsonObject = JSON.stringify(requestObject);
   var head = sessionInfo.basicAuthenticate;
   $.ajax({
     url : beUrl()+'/validateshopprop',
     type: 'POST',
     dataType:'json',
     processData:false,
     contentType: 'application/json',
     headers : {"basicauthenticate":""+head},
     data:finalJsonObject,
     success: function(response) {
       $('#promoForm').modal('open');
     },
     error: function (response) {
       Materialize.toast('Max active promo code already present', 4000);
     }
   });
 }
function promoAvail()
{   var myObj={};
    myObj['promoCode'] = document.getElementById('promoCode').value;
    console.log(myObj['promoCode']);
    var jsonObj = JSON.stringify(myObj);
    $.ajax({
                url : beUrl()+'/isnewpromo',
                type: 'POST',
                dataType:'json',
                processData:false,
                contentType: 'application/json',
                data:jsonObj,
                success: function(response) {
                   $("#promoCode").next("label").attr('data-success','Promocode Is valid');
                   $("#promoCode").removeClass("invalid");
                   $("#promoCode").addClass("valid");
                },
                error: function (response) {
                $('#promoCode').addClass("invalid");
                }
    });

}
function togglePromo(promoId, updateValue)
{
    if(updateValue == 1)
        {
            updateValue=0;
        }
    else if(updateValue ==0 )
        {
            updateValue=1;
        }

    var sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
    var myObj = {};

    var typeId = sessionInfo.typeId;
    var userId = sessionInfo.userId;
    var userType= sessionInfo.userType;
    var head = sessionInfo.basicAuthenticate;
    myObj['typeId'] = typeId;
    myObj['userId'] = userId;
    myObj['userType'] = userType;
    myObj['updateValue'] = updateValue;
    myObj['promoId'] = promoId;
    var jsonObj = JSON.stringify(myObj);
    $.ajax({
                url : beUrl()+'/updatepromo',
                type: 'POST',
                dataType:'json',
                processData:false,
                contentType: 'application/json',
                headers : {"basicauthenticate":""+head},
                data:jsonObj,
                success: function(response) {
                   Materialize.toast('Promo Updated', 4000);
                    setTimeout(function(){
                        location.reload();
                    }, 1000);
                },
                 error: function (response) {
                    var responseJson = JSON.stringify(response);
                    var responseJsonR = JSON.parse(responseJson);
                    var statusCode = responseJsonR.responseJSON;
                    var data = JSON.stringify(statusCode);
                    var xData = JSON.parse(data);
                    var message = xData.Data;
                    Materialize.toast(message, 4000);
                    console.log()
        }
    });
}
