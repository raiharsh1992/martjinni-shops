$(document).ready(function(){
    if(localStorage.getItem('sessionInfo')){
      var sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
      var orderId = getUrlVars()["orderId"];
      var myObj = {};
      myObj['orderId']=orderId;
      myObj['typeId']=sessionInfo.typeId;
      myObj['userId']=sessionInfo.userId;
      myObj['userType']=sessionInfo.userType;
      var jsonObj = JSON.stringify(myObj); 
      var head = sessionInfo.basicAuthenticate;
      $.ajax({
        url : beUrl()+'/orderdetails',
        type: 'POST',
        dataType:'json',
        processData:false,
        contentType: 'application/json',
        headers : {"basicauthenticate":""+head},
        data:jsonObj,
        success: function(response) {
          var jsonData = JSON.stringify(response);
          var dataUse = JSON.parse(jsonData);
          $('#orderId').append(dataUse.orderId);
          $('#orderStatus').append(dataUse.orderStatus);
          $('#createdOnDate').append(dataUse.creationDate);
          $('#paymentMethod').append(dataUse.paymentMethod);
          $('#addressName').append(dataUse.addName);
          var address = dataUse.addLine1+" "+dataUse.addLine2+" "+dataUse.city+" "+dataUse.state+" "+dataUse.country+" "+dataUse.pincode;
          $('#deliveryAddress').append(address);

          $('#totalAmount').append(dataUse.amount);
          $('#totalItems').append(dataUse.itemCount);
          var items = dataUse.items;
          $.each(items, function(i,v){
            var print = "<li><div class=collapsible-header><i class=material-icons>local_dining</i>"+v.itemName+"</div><div class=collapsible-body style='padding:0px'><span><table><thead><tr><th>Day</th><th>ETA</th><th>Status</th></tr></thead><tbody id="+v.itemId+"> </tbody></table></span></div></li>";
            $('#itemList').append(print);
            var itemHist = v.itemTranHistory;
            $.each(itemHist, function(k, l){
              var xPrint = "<tr><td>"+l.dayCount+"</td><td>"+l.orderEta+"</td><td>"+l.status+"</td></tr>";
              $('#'+v.itemId).append(xPrint);
            })
          })
          if((dataUse.orderStatus)=="NEW"){
            var xPrint = "<a href=javascript:acceptOrder("+dataUse.orderId+") class='btn green' style=' bottom:20px; right:20px; position:fixed;'>Accept Order</a>";
            var yPrint = "<a href=javascript:cancelOrder("+dataUse.orderId+","+dataUse.orderMasterId+") class='btn red' style='background-color:red; bottom:20px; left:20px; position:fixed;'>Cancel Order</a>";
            $('#buttonAccept').append(xPrint);
            $('#buttonCancel').append(yPrint);

          }
        },
        error: function(response){
          Materialize.toast('Not authorized to view the page', 4000)
          setTimeout(function(){
             window.location.replace(feUrl()+"/index.html");
          }, 500);
        }
      });
    }
    else{
      Materialize.toast('Not authorized to view the page', 4000)
      setTimeout(function(){
         window.location.replace(feUrl()+"/index.html");
      }, 500);
    }
  });

  function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
      vars[key] = value;
    });
    return vars;
  }

  function acceptOrder(orderId) {
    if(localStorage.getItem('sessionInfo')){
      var sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
      var myObj = {};
      myObj['userId'] = sessionInfo.userId;
      myObj['typeId'] = sessionInfo.typeId;
      myObj['userType'] = sessionInfo.userType;
      myObj['orderId'] = orderId;
      myObj['orderStatus'] = "ACCEPTED";
      var jsonObj = JSON.stringify(myObj);
      var head = sessionInfo.basicAuthenticate;
      $.ajax({
        url : beUrl()+'/updateorderstatus',
        type: 'POST',
        dataType:'json',
        processData:false,
        contentType: 'application/json',
        headers : {"basicauthenticate":""+head},
        data:jsonObj,
        success: function(response) {
          Materialize.toast('Order ACCEPTED Please wait', 4000)
          setTimeout(function(){
             window.location.replace(feUrl()+"/orders.html");
          }, 500);
        }
      });
    }
    console.log(orderId);
  }

  function cancelOrder(storeId, masterId) {
    if(localStorage.getItem('sessionInfo')){
      var sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
      var arr = [];
      arr.push(String(storeId));
      var myObj = {};
      myObj['typeId'] = sessionInfo.typeId;
      myObj['userId'] = sessionInfo.userId;
      myObj['userType'] = sessionInfo.userType;
      myObj['orderId'] = storeId;
      var jsonObj = JSON.stringify(myObj);
      var head = sessionInfo.basicAuthenticate;
      $.ajax({
        url : beUrl()+'/cancelorder',
        type: 'POST',
        dataType:'json',
        processData:false,
        contentType: 'application/json',
        headers : {"basicauthenticate":""+head},
        data:jsonObj,
        success: function(response) {
          Materialize.toast('Order Canceled Please wait', 4000)
          setTimeout(function(){
             window.location.replace(feUrl()+"/orders.html");
          }, 500);
        },
        error: function(resposne) {
          console.log(resposne);
        }
      });
    }
  }
