$(document).ready(function() {
    $('.modal').modal({
        dismissible: false, // Modal can be dismissed by clicking outside of the modal
        opacity: .5,
    });
    $('.modal').modal('open');
    $("#chngPwd").click(function(event){
      event.preventDefault();
      var passId = document.getElementById('password2');
      var passClass = passId.className;
      if(passClass=="validate valid"){
        var asObj = {};
        asObj['phoneNumber'] = document.getElementById('userPhone').value;
        asObj['userType'] = "SHP";
        asObj['otp'] = document.getElementById('otp').value;
        asObj['password'] = document.getElementById('password2').value;
        var jsonOtp = JSON.stringify(asObj);
        $.ajax({
            url: beUrl() + '/chngpwd',
            type: 'POST',
            dataType: 'json',
            processData: false,
            contentType: 'application/json',
            data: jsonOtp,
            success: function(response) {
              var useData = JSON.parse(JSON.stringify(response));
              if(useData.Data=="Update successful"){
                localStorage.clear();
                window.location.replace(feUrl()+"/index.html");
              }
              else{
                Materialize.toast('Kindly update password', 4000);
              }
            },
            error: function(response) {
                var responseJson = JSON.stringify(response);
                var responseJsonR = JSON.parse(responseJson);
                var statusCode = responseJsonR.responseJSON;
                var data = JSON.stringify(statusCode);
                var xData = JSON.parse(data);
                var message = xData.Data;
                Materialize.toast(message, 4000);
            }
        });
      }
      else{
        Materialize.toast('Kindly pass correct password', 4000);
      }
    });
    $("#otpClick").click(function(event){
      event.preventDefault();
      var asObj = {};
      asObj['phoneNumber']  = document.getElementById('userPhone').value;
      asObj['userType'] = "SHP";
      asObj['otp'] = document.getElementById('otp').value;
      var jsonOtp = JSON.stringify(asObj);
      console.log(jsonOtp);
      $.ajax({
          url: beUrl() + '/isvalidchangeotp',
          type: 'POST',
          dataType: 'json',
          processData: false,
          contentType: 'application/json',
          data: jsonOtp,
          success: function(response) {
            var useData = JSON.parse(JSON.stringify(response));
            if(useData.Data=="false"){
              $('.stepper').openStep(2);
              Materialize.toast('Incorrect OTP', 4000);
            }
            else{
              Materialize.toast('Kindly update password', 4000);
            }
          },
          error: function(response) {
              var responseJson = JSON.stringify(response);
              var responseJsonR = JSON.parse(responseJson);
              var statusCode = responseJsonR.responseJSON;
              var data = JSON.stringify(statusCode);
              var xData = JSON.parse(data);
              var message = xData.Data;
              Materialize.toast(message, 4000);
              $('.stepper').openStep(2);
          }
      });
    });
    $("#phonClick").click(function(){
      event.preventDefault();
      var asObj = {};
      asObj['phoneNumber'] = document.getElementById('userPhone').value;
      asObj['userType'] = "SHP";
      asObj['userNeed'] = "CHNGPWD";
      var jsonOtp = JSON.stringify(asObj);
      $.ajax({
          url: beUrl() + '/generateotp',
          type: 'POST',
          dataType: 'json',
          processData: false,
          contentType: 'application/json',
          data: jsonOtp,
          success: function(response) {
              Materialize.toast('Please see your phone for OTP', 4000);
          },
          error: function(response) {
              var responseJson = JSON.stringify(response);
              var responseJsonR = JSON.parse(responseJson);
              var statusCode = responseJsonR.responseJSON;
              var data = JSON.stringify(statusCode);
              var xData = JSON.parse(data);
              var message = xData.Data;
              Materialize.toast(message, 4000);
              $('.stepper').openStep(1);
          }
      });
    });
});


function phoneAvailablity() {
    var validatePhone = document.getElementById('userPhone');
    var inputPhone = validatePhone.value;
    var length = validatePhone.value.length;
    if (length < 10) {

        $("#userPhone").next("label").attr('data-error', 'Too small phone number');
        $("#userPhone").removeClass("valid");
        $("#userPhone").addClass("invalid");
        $('.pnnumberval').addClass("disabled");
    } else if (length > 10) {
        $('.pnnumberval').addClass("disabled");
        $("#userPhone").next("label").attr('data-error', 'Too big phone number');
        $("#userPhone").removeClass("valid");
        $("#userPhone").addClass("invalid");
    } else { //Number is 10 digit
        if(localStorage.getItem('sessionInfo')){
          var sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
          var myObj = {};
          myObj['userId'] = sessionInfo.userId;
          myObj['typeId'] = sessionInfo.typeId;
          myObj['userType'] = sessionInfo.userType;
          myObj['phoneNumber'] = document.getElementById('userPhone').value;
          var data = JSON.stringify(myObj);
          var head = sessionInfo.basicAuthenticate;
          console.log('here');
          $.ajax({
              url: beUrl() + '/isvaliduserphone',
              type: 'POST',
              dataType: 'json',
              processData: false,
              contentType: 'application/json',
              headers: {
                  "basicauthenticate": "" + head
              },
              data: data,
              success: function(response) {
                var jData = JSON.parse(JSON.stringify(response));
                if(jData.Data=="true"){
                  $("#phonClick").removeClass("disabled");
                  $("#userPhone").removeClass("invalid");
                  Materialize.toast('Valid phone number', 4000)
                  console.log(jData);
                }
                else{
                  Materialize.toast('Not valid phone number', 4000)
                }
              }
          });
        }
        else{
          $("#userPhone").removeClass("invalid");
          $("#phonClick").removeClass("disabled");
        }
    }
}

function validPass() {
  var pass = document.getElementById('password1').value;
  if(pass.length<8 || pass.length>24){
    $("#password1").removeClass("valid");
    $("#password1").addClass("invalid");
  }
  else{
    $("#password1").removeClass("invalid");
    $("#password1").addClass("valid");
  }
}

function isRealPass() {
  var pass1 = document.getElementById('password1').value;
  var pass2 = document.getElementById('password2').value;
  if(pass1==pass2){
    $("#password2").removeClass("invalid");
    $("#password2").addClass("valid");
  }
  else{
    $("#password2").removeClass("valid");
    $("#password2").addClass("invalid");
  }
}
