$(document).ready(function(){
  $('.modal').modal({
    dismissible: true, // Modal can be dismissed by clicking outside of the modal
    opacity: .5,
  });
 $('#updateSubItemIsActivee').material_select();
  if(localStorage.getItem("useGst")){
    $.ajax({
      url : beUrl()+'/getgstratelist',
      type: 'POST',
      dataType:'json',
      processData:false,
      contentType: 'application/json',
      success: function(response) {
        var jSresponse = JSON.stringify(response);
        var useJsResponse = JSON.parse(jSresponse);
        var data = useJsResponse.data;
        $.each(data, function(i,v){
          var jPrint = "<option value="+v.gstId+">"+v.gstName+"</option>";
          $('#gstInfo').append(jPrint);

        });
        $('select').material_select();
      }
    });
  }else{
    $('#gstShow').hide();
  }

  var tMyObj = {};
  tMyObj['viewType']="items";
  var jsonDummy = JSON.stringify(tMyObj);
  var autoData = {};
  $.ajax({
    url : beUrl()+'/viewcateogary',
    type: 'POST',
    dataType:'json',
    processData:false,
    contentType: 'application/json',
    data:jsonDummy,
    success: function(response) {
      var jSresponse = JSON.stringify(response);
      var useJsResponse = JSON.parse(jSresponse);
      var data = useJsResponse.data;
      $.each(data, function(i,v){
        var jPrint = "<option value="+v.catId+">"+v.catName+"</option>";
        $('#catInfo').append(jPrint);
        autoData[v.catName]=null;
      });
      $('select').material_select();
      var jsonAuto = JSON.parse(JSON.stringify(autoData));
      $('input.autocomplete').autocomplete({
        data: jsonAuto,
        limit: 5, // The max amount of results that can be shown at once. Default: Infinity.
        minLength: 1, // The minimum length of the input for the autocomplete to start. Default: 1.
      });
    }
  });
  Materialize.updateTextFields();
  var sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
  var shopId = sessionInfo.typeId;
  var myObj = {};
  myObj['shopId']=shopId;
  var data = JSON.stringify(myObj);
  $.ajax({
    url : beUrl()+'/itemlist',
    type: 'POST',
    dataType:'json',
    processData:false,
    contentType: 'application/json',
    data:data,
    success: function(response) {
      var storeItem = JSON.stringify(response);
      var storeItemData = JSON.parse(storeItem);
      var totalCount = storeItemData.totalCount;
      if(totalCount>0){
        var itemData = storeItemData.data;
        var x = 0;
        $.each(itemData, function(i,v){
          var mainItemId = v.mainItemId;
          var mainItemName = v.Name;
          var subItems = v.subItems;
          var print = "<div class='collapsible-header as_head'><i class=material-icons>local_dining</i>"+mainItemName+"</div>"
          var xPrint = "";
          $.each(subItems, function(k, l) {
              var subItemName = l.Name;
              var subItemId = l.subItemId;
              
              if(l.isActive == 0)
                  {
                      xPrint = xPrint+"<tr><td style='color:#5b1515' id="+subItemId+"Name>"+subItemName+" [Disabled]</td><td><a class='btn-small waves-effect waves-light btn-outline' href=javascript:workWithQuantity("+mainItemId+","+subItemId+")><i class='material-icons' style='color:;'>build</i></a></td><td><a class='btn-small waves-effect waves-light btn-outline' href=javascript:uploadImageItem("+subItemId+")><i class='material-icons' style='color:;'>camera_alt</i></a></td><td><a class='btn-small waves-effect waves-light btn-outline' href=javascript:updateSubItem("+mainItemId+","+subItemId+","+l.isActive+")><i class='material-icons' style='color:;'>edit</i></a></td></tr>";
                  }
              else{
                  xPrint = xPrint+"<tr><td id="+subItemId+"Name>"+subItemName+"</td><td><a class='btn-small waves-effect waves-light btn-outline' href=javascript:workWithQuantity("+mainItemId+","+subItemId+")><i class='material-icons' style='color:;'>build</i></a></td><td><a class='btn-small waves-effect waves-light btn-outline' href=javascript:uploadImageItem("+subItemId+")><i class='material-icons' style='color:;'>camera_alt</i></a></td><td><a class='btn-small waves-effect waves-light btn-outline' href=javascript:updateSubItem("+mainItemId+","+subItemId+","+l.isActive+")><i class='material-icons' style='color:;'>edit</i></a></td></tr>";
              }
              var quantInfo = l.quantInfo;
              var jsonDataUsed = JSON.stringify(quantInfo);
              var subName = "sub"+subItemId;
              localStorage.setItem(subName,jsonDataUsed)
              localStorage.setItem(subItemId,l.Name)
          });
          var jPrint = "<div class=collapsible-body style='padding:0px'><span><table><thead><tr><th>Name</th><th></th><th></th><th></th></tr></thead><tbody>"+xPrint+"</tbody></table></span><div style='text-align:center; padding:5px;'><a class='btn-floating waves-effect waves-light btn-outline'  href=javascript:addSubItem("+mainItemId+")><i class=material-icons style='color:;'>add</i></a></div></div>"
          print = "<li>"+print+jPrint+"</li>"
          $('#itemList').append(print);
        });
        var lPrint = "<div class='col s2 offset-s5 l2 offset-l5 m2 offset-m5 xl2 offset-xl5'><a class='btn-floating btn-large waves-effect waves-light main_btn_down' style='background-color:#ffa751; position:fixed; bottom:20px; left:20px;' href=javascript:addMainItem()><i class='material-icons' style='color:;'>add</i></a></div>";
        $('body').append(lPrint);
      }
      else{
        var $toastContent = $('<span>No items by the store</span>').add($('<button class="btn-flat toast-action" onclick="addMainItem()">Add</button><button class="btn-flat toast-action" onclick="redirect()">Back</button>'));
        Materialize.toast($toastContent);
      }
    }
  });
  $.ajax({
    url : beUrl()+'/shopaddoninfo',
    type: 'POST',
    dataType:'json',
    processData:false,
    contentType: 'application/json',
    data:data,
    success: function(response) {
      var dispData = JSON.parse(JSON.stringify(response)).data;
      $.each(dispData, function(i,v){
        var printx = "<option value="+v.groupId+">"+v.groupName+"</options>"
        $("#addOnInfo").append(printx);
      });
    },
    error: function(response) {
      $("#addOnInfoId").addClass("hide");
    }
  });
  $.ajax({
    url : beUrl()+'/shopsubsinfo',
    type: 'POST',
    dataType:'json',
    processData:false,
    contentType: 'application/json',
    data:data,
    success: function(response) {
      var dispData = JSON.parse(JSON.stringify(response)).data;
      $.each(dispData, function(i,v){
        var printx = "<option value="+v.subscriptionId+">"+v.noOfDays+"</options>"
        $("#subsInfo").append(printx);
      });
    },
    error: function(response) {
      $("#subsInfoId").addClass("hide");
    }
  });
     $("#addLabelMAin").submit(function(event){
          event.preventDefault();
          $("#mainItem").click();
      });
  $("#mainItem").click(function(){
    var sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
    var myObj = {};
    var mainItemNameId = document.getElementById('autocomplete-input');
      myObj['itemCatId'] = Number(document.getElementById('catSelect').value);
      myObj['typeId'] = sessionInfo.typeId;
      myObj['userId'] = sessionInfo.userId;
      myObj['userType'] = sessionInfo.userType;
      myObj['mItemName'] = mainItemNameId.value;
      var jsonObj = JSON.stringify(myObj);
      var head = sessionInfo.basicAuthenticate;
      $.ajax({
        url : beUrl()+'/addmainitem',
        type: 'POST',
        dataType:'json',
        processData:false,
        contentType: 'application/json',
        headers : {"basicauthenticate":""+head},
        data:jsonObj,
        success: function(response) {
          Materialize.toast('cateogary created', 4000);
          setTimeout(function(){
            location.reload();
          }, 1000);
        },
        error: function (response) {
          var responseJson = JSON.stringify(response);
          var responseJsonR = JSON.parse(responseJson);
          var statusCode = responseJsonR.responseJSON;
          var data = JSON.stringify(statusCode);
          var xData = JSON.parse(data);
          var message = xData.Data;
          Materialize.toast(message, 4000);
        }
      });
  });
    
    $("#addingSubItem").submit(function(event){
          event.preventDefault();
          $("#subItem").click();
      });
  $("#subItem").click(function(){
    var sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
    var myObj = {};
    var subItemNameId = document.getElementById('subItemName');
    var subItemRateId = document.getElementById('subItemDesc');
    if((subItemNameId.className=="validate valid")&&(subItemRateId.className=="validate valid")){
      myObj['typeId'] = sessionInfo.typeId;
      myObj['userId'] = sessionInfo.userId;
      myObj['userType'] = sessionInfo.userType;
      myObj['subItemName'] = subItemNameId.value;
      myObj['itemDesc'] = subItemRateId.value;
      myObj['addOnInfo'] = $('#addOnInfo').val();
      myObj['subsInfo'] = $('#subsInfo').val();
      myObj['mItemId'] = localStorage.getItem('subItemMainItemId')
      if(localStorage.getItem("useGst")){
          myObj['gstId'] = document.getElementById('gstInfo').value;
      }
      else{
          myObj['gstId'] = 1
      }
      myObj['spclCat'] = document.getElementById('spclCat').value;
      var jsonObj = JSON.stringify(myObj);
      var head = sessionInfo.basicAuthenticate;
      $.ajax({
        url : beUrl()+'/addsubitem',
        type: 'POST',
        dataType:'json',
        processData:false,
        contentType: 'application/json',
        headers : {"basicauthenticate":""+head},
        data:jsonObj,
        success: function(response) {
          Materialize.toast('Item added', 4000);
          setTimeout(function(){
            location.reload();
          }, 1000);
        },
        error: function (response) {
          var responseJson = JSON.stringify(response);
          var responseJsonR = JSON.parse(responseJson);
          var statusCode = responseJsonR.responseJSON;
          var data = JSON.stringify(statusCode);
          var xData = JSON.parse(data);
          var message = xData.Data;
          Materialize.toast(message, 4000);
        }
      });
    }
    else{
      Materialize.toast('Kindly pass cateogary name',4000);
    }
  });
  $("#updaterSubItem").click(function(){
    var sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
    var myObj = {};
    var subItemNameId = document.getElementById('UpdateSubItemName');
    var subItemRateId = document.getElementById('updateSubItemIsActivee');
    if((subItemNameId.className=="validate valid")){
      myObj['typeId'] = sessionInfo.typeId;
      myObj['userId'] = sessionInfo.userId;
      myObj['userType'] = sessionInfo.userType;
      myObj['subItemName'] = subItemNameId.value;
      myObj['mItemId'] = localStorage.getItem('updateMainItemId');
      myObj['updateType'] = "subItem";
      myObj['subItemId'] = localStorage.getItem('updateSubItemId');
      myObj['isActiveFl'] = Number(subItemRateId.value);
      var jsonObj = JSON.stringify(myObj);
      var head = sessionInfo.basicAuthenticate;
      $.ajax({
        url : beUrl()+'/updateitem',
        type: 'POST',
        dataType:'json',
        processData:false,
        contentType: 'application/json',
        headers : {"basicauthenticate":""+head},
        data:jsonObj,
        success: function(response) {
          Materialize.toast('Item updated', 4000);
          setTimeout(function(){
            location.reload();
          }, 1000);
        },
        error: function (response) {
          var responseJson = JSON.stringify(response);
          var responseJsonR = JSON.parse(responseJson);
          var statusCode = responseJsonR.responseJSON;
          var data = JSON.stringify(statusCode);
          var xData = JSON.parse(data);
          var message = xData.Data;
          Materialize.toast(message, 4000);
        }
      });
    }
    else{
      Materialize.toast('Kindly pass cateogary name',4000);
    }
  });
  $("#quantityC").click(function(){
    var sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
    var myObj = {};
    var typeId = sessionInfo.typeId;
    var userId = sessionInfo.userId;
    var userType= sessionInfo.userType;
    var mItemId = localStorage.getItem('workingQuantMItemId');
    var subItemId = localStorage.getItem('workingQuantSubItemId');
      myObj['typeId'] =typeId;
      myObj['userId'] = userId;
      myObj['userType'] = userType;
      myObj['subItemName'] = subItemId.value;
      myObj['mItemId'] = mItemId;
      myObj['subItemId'] = subItemId;
      myObj['totalQuantInfo'] = Number($('#selectOpt').val());
      myObj['quantInfo']=[];
      for (i=1;i<=$('#selectOpt').val();i++){
        var myUseObj = {};
        myUseObj['quantity'] =document.getElementById("qnname"+i).value;
        console.log('From insi`de '+myUseObj['quantity']);
        myUseObj['value'] = Number(document.getElementById("qnvalue"+i).value);
        myUseObj['mrp'] = Number(document.getElementById("qnmrp"+i).value);
        var jsonUsedObj = myUseObj;
        myObj['quantInfo'].push(jsonUsedObj);
      }
      var jsonObj = JSON.stringify(myObj);
      var head = sessionInfo.basicAuthenticate;
      $.ajax({
        url : beUrl()+'/insertquantityinfo',
        type: 'POST',
        dataType:'json',
        processData:false,
        contentType: 'application/json',
        headers : {"basicauthenticate":""+head},
        data:jsonObj,
        success: function(response) {
          Materialize.toast('Item updated', 4000);
          setTimeout(function(){
            location.reload();
          }, 1000);
        },
        error: function (response) {
          var responseJson = JSON.stringify(response);
          var responseJsonR = JSON.parse(responseJson);
          var statusCode = responseJsonR.responseJSON;
          var data = JSON.stringify(statusCode);
          var xData = JSON.parse(data);
          var message = xData.Data;
          Materialize.toast(message, 4000);
        }
      });
  });
     $("#updateQuantityInfo").submit(function(event){
          event.preventDefault();
          $("#updaterQuantityItem").click();
      });
  $("#updaterQuantityItem").click(function(){
    var sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
    var myObj = {};
    var quantity = document.getElementById('UpdateQuantName');
    var value = document.getElementById('updateQuantityValue');
    var mrp = document.getElementById('updateQuantityMrp');
    if((quantity.className=="validate valid")&&(value.className=="validate valid")&&(mrp.className=="validate valid")){
      myObj['typeId'] = sessionInfo.typeId;
      myObj['userId'] = sessionInfo.userId;
      myObj['userType'] = sessionInfo.userType;
      myObj['mItemId'] = localStorage.getItem('workingQuantMItemId');
      myObj['subItemId'] = localStorage.getItem('workingQuantSubItemId');
      myObj['quantId'] = localStorage.getItem('workingQuantId');
      myObj['updateType'] = "quantity";
      myObj['quantity'] = quantity.value;
      myObj['value'] = value.value;
      myObj['mrp'] = mrp.value;
      var jsonObj = JSON.stringify(myObj);
      var head = sessionInfo.basicAuthenticate;
      $.ajax({
        url : beUrl()+'/updateitem',
        type: 'POST',
        dataType:'json',
        processData:false,
        contentType: 'application/json',
        headers : {"basicauthenticate":""+head},
        data:jsonObj,
        success: function(response) {
          Materialize.toast('Item updated', 4000);
          setTimeout(function(){
            location.reload();
          }, 1000);
        },
        error: function (response) {
          var responseJson = JSON.stringify(response);
          var responseJsonR = JSON.parse(responseJson);
          var statusCode = responseJsonR.responseJSON;
          var data = JSON.stringify(statusCode);
          var xData = JSON.parse(data);
          var message = xData.Data;
          Materialize.toast(message, 4000);
        }
      });
    }
    else{
      Materialize.toast('Kindly pass cateogary name',4000);
    }
  });
  var mainCat = {};
  mainCat['shopId'] = JSON.parse(localStorage.getItem("sessionInfo")).typeId;
  var jsonDummy = JSON.stringify(mainCat);
  $.ajax({
    url : beUrl()+'/showshopitemcat',
    type: 'POST',
    dataType:'json',
    processData:false,
    contentType: 'application/json',
    data:jsonDummy,
    success: function(response) {

      var jSresponse = JSON.stringify(response);
      var useJsResponse = JSON.parse(jSresponse);
      var data = useJsResponse.data;
      var catIdused=[];
      $.each(data, function(i,v){
        var catList = v.catItemList;
        if(jQuery.inArray(v.catId, catIdused) == -1)
        {
        catIdused.push(v.catId);

        $.each(catList, function(j,k){
          var printx = "<option value="+k.itemCatId+">"+k.itemCatName+"</options>"
          $("#catSelect").append(printx);
        });
      }
      });
      $('select').material_select();
      var jsonAuto = JSON.parse(JSON.stringify(autoData));
      $('input.autocomplete').autocomplete({
        data: jsonAuto,
        limit: 5, // The max amount of results that can be shown at once. Default: Infinity.
        minLength: 1, // The minimum length of the input for the autocomplete to start. Default: 1.
      });
    }
  });
});

function getUrlVars() {
  var vars = {};
  var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
    vars[key] = value;
  });
  return vars;
}

function redirect(){
  window.location.replace(feUrl()+"/index.html");
}

function addMainItem() {
  $('#addMainItem').modal('open');
}

function addSubItem(mainItemId) {
  localStorage.setItem('subItemMainItemId',mainItemId);
  $('#addSubItem').modal('open');
}

function updateSubItem(mainItemId,subItemId,isActive) {
  localStorage.setItem('updateMainItemId',mainItemId);
  localStorage.setItem('updateSubItemId',subItemId);
  document.getElementById('UpdateSubItemName').value = localStorage.getItem(subItemId);
  document.getElementById('updateSubItemIsActivee').value = isActive;
  $('#updateSubItem').modal('open');
  $("#UpdateSubItemName").focus();
  $("#updateSubItemIsActivee").focus();
}

function uploadImageItem(subItemId) {
  localStorage.setItem("subItemIdForImageUpload",subItemId);
  $('#uploadImage').modal('open');
}

function finalUpload() {
  if((localStorage.getItem("subItemIdForImageUpload"))&&(localStorage.getItem("sessionInfo"))){
    var sessionInfo = JSON.parse(localStorage.getItem("sessionInfo"));
    var subItemId = localStorage.getItem("subItemIdForImageUpload");
    var form = new FormData();
    var head = sessionInfo.basicAuthenticate;
    form.append("userId", sessionInfo.userId);
    form.append("typeId", sessionInfo.typeId);
    form.append("userType", sessionInfo.userType);
    form.append("proPic", $( '#file' )[0].files[0]);
    form.append("subItemId", subItemId);
    var settings = {
      "async": true,
      "crossDomain": true,
      "url": beUrl()+"/insertitemimage",
      "method": "POST",
      "headers": {
      "basicauthenticate": ""+head,
      },
      "processData": false,
      "contentType": false,
      "mimeType": "multipart/form-data",
      "data": form
    }
    $.ajax(settings).done(function (response) {
      Materialize.toast('Please wait for magic', 4000);
      setTimeout(function(){
        location.reload();
      }, 1000);
    }).fail(function(response){
      Materialize.toast("Not allowed", 4000);
      $('#uploadImage').modal('close');
    });
  }else{
    window.location.reload();
  }
}
function workWithQuantity(mainItemId,subItemId) {
  localStorage.setItem("workingQuantMItemId",mainItemId);
  localStorage.setItem("workingQuantSubItemId",subItemId);
  var subName = "sub"+subItemId;
  if(localStorage.getItem(subName)){
    var quantInfo = JSON.parse(localStorage.getItem(subName));
    if(quantInfo.length>0){
        document.getElementById('quantityInformation').innerHTML = "";
        $.each(quantInfo, function(i,v){
          var print = "<tr><td>"+v.quantity+"</td><td>"+v.mrp+"</td><td>"+v.value+"</td><td><a class='btn-small waves-effect waves-light btn-outline' href=javascript:updateQuantity("+v.quantId+","+subItemId+")><i class='material-icons' style='color:;'>build</i></a></td></tr>";
          $("#quantityInformation").append(print);
        });
        $('#quantityInfoModal').modal('open');
    }
    else{
      $("#quantityNew").modal('open');
      var propertyVal = localStorage.getItem('propertyVal');
      for (var i=1; i<=propertyVal; i++)
      {
        var printx = "<option value="+i+">"+i+"</option>"
        $("#selectOpt").append(printx);
      }
      $('select').material_select();
      var formFill ='<form class="col s12" id="quantityNew1"><div class="row"><div class="input-field col s12 l4"><input id="qnname1" type="text" class="validate" required  ><label for="qnname1" data-error="Needed">Quantity Name</label></div><div class="input-field col s12 l4"><input id="qnmrp1" type="number" class="validate" required  ><label  for="qnmrp1" data-error="Needed">Quantity MRP</label></div><div class="input-field col col s12 l4"><input id="qnvalue1" type="number" class="validate" required  ><label  for="qnvalue1" data-error="Needed">Quantity Value</label></div></div></form> <ht> <hr style="width:60%; text-align:center;">';
      $("#formFill").append(formFill);
      $('#selectOpt').change( function(){
        var selectedVal=$("#selectOpt").val();
        console.log(selectedVal);
        document.getElementById("formFill").innerHTML="";
        for(var i=1; i<=selectedVal; i++)
        { console.log(selectedVal);
          var formFill ='<form class="col s12" id="quantityNew'+i+'"><div class="row"><div class="input-field col s12 l4"><input id="qnname'+i+'" type="text" class="validate" required  ><label for="qnname'+i+'" data-error="Needed">Quantity Name</label></div><div class="input-field col s12 l4"><input id="qnmrp'+i+'" type="number" class="validate" required  ><label  for="qnmrp'+i+'" data-error="Needed">Quantity MRP</label></div><div class="input-field col col s12 l4"><input id="qnvalue'+i+'" type="number" class="validate" required  ><label  for="qnvalue'+i+'" data-error="Needed">Quantity Value</label></div></div></form>';
          $("#formFill").append(formFill);
        }
      });



    }
  }else{
    window.location.reload();
  }
}

function updateQuantity(quantId, subItemId) {
  localStorage.setItem("workingQuantId",quantId);
  var subName = "sub"+subItemId;
  if(localStorage.getItem(subName)){
    var quantInfo = JSON.parse(localStorage.getItem(subName));
    if(quantInfo.length>0){
        $.each(quantInfo, function(i,v){
          if(v.quantId==quantId){
            document.getElementById('UpdateQuantName').value = v.quantity;
            document.getElementById('updateQuantityMrp').value = v.mrp;
            document.getElementById('updateQuantityValue').value = v.value;
          }
        });
        $('#updateQuantInfo').modal('open');
        $("#UpdateQuantName").focus();
        $("#updateQuantityMrp").focus();
        $("#updateQuantityValue").focus();
    }
  }else{
    window.location.reload();
  }
}
