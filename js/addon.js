$(document).ready(function(){
  $('.modal').modal({
    dismissible: true, // Modal can be dismissed by clicking outside of the modal
    opacity: .5,
  });
    $('#updateIsActiveFlInofItem').material_select();
    $('#isMultiAddOns').material_select();
    $('#isActiveAddOns').material_select();
  if(localStorage.getItem("sessionInfo")){
    var sessionInfo = JSON.parse(localStorage.getItem("sessionInfo"));
    var myObj = {};
    myObj['shopId'] = sessionInfo.typeId;
    var jsonObj = JSON.stringify(myObj);
    $.ajax({
      url : beUrl()+'/shopaddoninfo',
      type: 'POST',
      dataType:'json',
      processData:false,
      contentType: 'application/json',
      data:jsonObj,
      success: function(response) {
        var data = JSON.parse(JSON.stringify(response)).data;
        $.each(data, function(i,v){
          var groupName = v.groupName;
          var groupCharge = v.groupCharge;
          var isMultiSelect = v.isMultiSelect;
          var groupId = v.groupId;
          var itemInfo = v.itemInfo;
          var isActive = v.isActiveFl;
          var dummG = "G"+groupId;
          localStorage.setItem(dummG,groupName);
          var print = "<div class='collapsible-header as_head' style='position:relative;'><i class=material-icons>local_dining</i>"+groupName+" @"+groupCharge+"Rs</div>"
          var xPrint = "";
          $.each(itemInfo, function(k, l) {
              var itemName = l.addOnItemName;
              var isActiveFl = l.isActiveFl;
              var dummI = "I"+l.addOnItemId;
              var _status="";
              if(isActiveFl==0)
                  {
                      _status="Disabled";
                  }
              else{
                 _status="Active";
              }
              localStorage.setItem(dummI,itemName);
              xPrint = xPrint+"<tr><td id="+itemName+"Name>"+itemName+"</td><td id="+itemName+"isActive>"+_status+"</td><td><a class='btn-small waves-effect waves-light btn-outline' href=javascript:updateAddOnItem("+groupId+","+l.addOnItemId+","+isActive+")><i class='material-icons' >edit</i></a></td></tr>";
          });
          var jPrint = "<div class=collapsible-body style='padding:0px'><span><table><thead><tr><th>Item</th><th>Status</th><th>Edit Status</th></tr></thead><tbody>"+xPrint+"</tbody><div><a class='btn waves-effect waves-light btn-outline'  href=javascript:addAddOnItem("+groupId+") style='float:left; padding:5px; margin-bottom:5px;'><i class='material-icons right'>add</i>Add-on</a><a class='btn waves-effect waves-light ' style='float:right; padding:5px;margin:5px;' onclick='javascript:updateAddOnGroup("+groupCharge+","+isActive+","+isMultiSelect+","+groupId+")'><i class='material-icons left'>edit</i>Edit</a></div></table></span></div>"
          print = "<li>"+print+jPrint+"</li>"
          $('#addOnList').append(print);
        });
      }
    });
    $("#updaterAddOnInfo").click(function(){
      var xmyObj = {};
      xmyObj['addOnName']=document.getElementById('updateGroupName').value;
      xmyObj['addOnCharge']=document.getElementById('updateGroupPrice').value;
      xmyObj['isActiveFl']=document.getElementById('isMultiAddOns').value;
      xmyObj['isMultiSelect']=document.getElementById('isActiveAddOns').value;
      xmyObj['userId']=sessionInfo.userId;
      xmyObj['typeId']=sessionInfo.typeId;
      xmyObj['userType']=sessionInfo.userType;
      var head = sessionInfo.basicAuthenticate;
      var useJson = JSON.stringify(xmyObj);
      $.ajax({
        url : beUrl()+'/updateaddon',
        type: 'POST',
        dataType:'json',
        processData:false,
        contentType: 'application/json',
        headers : {"basicauthenticate":""+head},
        data:useJson,
        success: function(response) {
          Materialize.toast('Add-On updated', 4000);
          setTimeout(function(){
            location.reload();
          }, 1000);
        },
        error: function (response) {
          var responseJson = JSON.stringify(response);
          var responseJsonR = JSON.parse(responseJson);
          var statusCode = responseJsonR.responseJSON;
          var data = JSON.stringify(statusCode);
          var xData = JSON.parse(data);
          var message = xData.Data;
          Materialize.toast(message, 4000);
        }
      });
    });
      
      $("#insertingInfoAddon").submit(function(event){
           event.preventDefault();
          $("#insertorAddOnInfo").click();
      });
    $("#insertorAddOnInfo").click(function(){
      var xmyObj = {};
      xmyObj['addOnItemName']=document.getElementById('isertAddOnItemName').value;
      xmyObj['addOnId']=Number(localStorage.getItem("addOnForInsert"));
      xmyObj['userId']=sessionInfo.userId;
      xmyObj['typeId']=sessionInfo.typeId;
      xmyObj['userType']=sessionInfo.userType;
      var head = sessionInfo.basicAuthenticate;
      var useJson = JSON.stringify(xmyObj);
      $.ajax({
        url : beUrl()+'/iteminsertaddon',
        type: 'POST',
        dataType:'json',
        processData:false,
        contentType: 'application/json',
        headers : {"basicauthenticate":""+head},
        data:useJson,
        success: function(response) {
          Materialize.toast('Add-On updated', 4000);
          setTimeout(function(){
            location.reload();
          }, 1000);
        },
        error: function (response) {
          var responseJson = JSON.stringify(response);
          var responseJsonR = JSON.parse(responseJson);
          var statusCode = responseJsonR.responseJSON;
          var data = JSON.stringify(statusCode);
          var xData = JSON.parse(data);
          var message = xData.Data;
          Materialize.toast(message, 4000);
        }
      });
    });
      $("#updatingInfoAddonItem").submit(function(event){
          event.preventDefault();
          $("#updatorAddOnInfoItem").click();
      });
    $("#updatorAddOnInfoItem").click(function(){
      var xmyObj = {};
      xmyObj['addOnItemName']=document.getElementById('updateAddOnItemNameInfo').value;
      xmyObj['isActiveFl']=document.getElementById('updateIsActiveFlInofItem').value;
      xmyObj['addOnId']=Number(localStorage.getItem("addOnIdForItemUpdate"));
      xmyObj['userId']=sessionInfo.userId;
      xmyObj['typeId']=sessionInfo.typeId;
      xmyObj['userType']=sessionInfo.userType;
      var head = sessionInfo.basicAuthenticate;
      var useJson = JSON.stringify(xmyObj);
      $.ajax({
        url : beUrl()+'/itemupdateaddon',
        type: 'POST',
        dataType:'json',
        processData:false,
        contentType: 'application/json',
        headers : {"basicauthenticate":""+head},
        data:useJson,
        success: function(response) {
          Materialize.toast('Add-On updated', 4000);
          setTimeout(function(){
            location.reload();
          }, 1000);
        },
        error: function (response) {
          var responseJson = JSON.stringify(response);
          var responseJsonR = JSON.parse(responseJson);
          var statusCode = responseJsonR.responseJSON;
          var data = JSON.stringify(statusCode);
          var xData = JSON.parse(data);
          var message = xData.Data;
          Materialize.toast(message, 4000);
        }
      });
    });
      
      $("#insertingInfoAddon").submit(function(event){
          event.preventDefault();
          $("#creationAddOnInfo").click();
      });
    $("#creationAddOnInfo").click(function(){
      var xmyObj = {};
      xmyObj['addOnName']=document.getElementById('insertGroupName').value;
      xmyObj['addOnCharge']=document.getElementById('insertGroupPrice').value;
      xmyObj['isMultiSelect']=document.getElementById('isMultiAddOnsInsert').value;
      xmyObj['userId']=sessionInfo.userId;
      xmyObj['typeId']=sessionInfo.typeId;
      xmyObj['userType']=sessionInfo.userType;
      var head = sessionInfo.basicAuthenticate;
      var useJson = JSON.stringify(xmyObj);
      $.ajax({
        url : beUrl()+'/insertaddon',
        type: 'POST',
        dataType:'json',
        processData:false,
        contentType: 'application/json',
        headers : {"basicauthenticate":""+head},
        data:useJson,
        success: function(response) {
          Materialize.toast('Add-On updated', 4000);
          setTimeout(function(){
            location.reload();
          }, 1000);
        },
        error: function (response) {
          var responseJson = JSON.stringify(response);
          var responseJsonR = JSON.parse(responseJson);
          var statusCode = responseJsonR.responseJSON;
          var data = JSON.stringify(statusCode);
          var xData = JSON.parse(data);
          var message = xData.Data;
          Materialize.toast(message, 4000);
        }
      });
    });
  }
  else{
    window.location.replace(feUrl()+"/index.html");
  }
});


function updateAddOnGroup(groupCharge,isActive,isMultiSelect,groupId) {
  var dummG = "G"+groupId;
  var groupName = localStorage.getItem(dummG);
  document.getElementById('updateGroupName').value = groupName;
  document.getElementById('updateGroupPrice').value = groupCharge;
  document.getElementById('isMultiAddOns').value = isMultiSelect;
  document.getElementById('isActiveAddOns').value = isActive;
  $('#updateAddOnInfo').modal('open');
  $("#updateGroupName").focus();
  $("#updateGroupPrice").focus();
  $("#isActiveAddOns").focus();
  $("#isMultiAddOns").focus();
}

function addAddOnItem(groupId) {
  localStorage.setItem("addOnForInsert",groupId);
  $('#insertAddOnInfo').modal('open');
}

function updateAddOnItem(groupId,addOnItemId,isActive) {
  var dummI = "I"+addOnItemId;
  localStorage.setItem("addOnIdForItemUpdate",groupId)
  document.getElementById('updateAddOnItemNameInfo').value = localStorage.getItem(dummI);
  document.getElementById('updateIsActiveFlInofItem').value = isActive;
  $('#updateAddOnInfoItem').modal('open');
  $("#updateAddOnItemNameInfo").focus();
  $("#updateIsActiveFlInofItem").focus();
}
