$(document).ready(function(){
  $('.modal').modal({
    dismissible: false, // Modal can be dismissed by clicking outside of the modal
    opacity: .5,
  })
  $('#contactModal').modal('open');
  $("#contactForm").submit(function(event){
    event.preventDefault();
    $("#contactButton").click();
  })
$("#contactButton").click(function(){
  event.preventDefault();
  var userName = document.getElementById('userName').value;
  var phoneNumber = document.getElementById('phoneNumber').value;
  var email = document.getElementById('email').value;
  var address = document.getElementById('address').value;
  var message = document.getElementById('message').value;
  var myObj= {};
    myObj['userName']=userName;
    myObj['userType']="CUST";
    myObj['message']=message;
    myObj['phoneNumber']=phoneNumber;
    myObj['address']=address;
    myObj['email']=email;
    var jsonObj = JSON.stringify(myObj);
$.ajax({
  url: beUrl()+'/makequery',
  type: 'POST',
  dataType:'json',
  processData:false,
  contentType: 'application/json',
  data:jsonObj,
  success: function(response)
    {
        Materialize.toast('Thankyou. We will get back to you soon.', 4000);
        setTimeout(function(){
            location.reload();
        }, 2000);
    },
  error: function(response){
      var res = JSON.stringify(response);
      var res1 = JSON.parse(res);
      var res2 = res1.Data;
    Materialize.toast('Error. Try again later', 4000);
  }
});
})
});

