$(document).ready(function() {
    $('.modal').modal({
        dismissible: true, // Modal can be dismissed by clicking outside of the modal
        opacity: .5,
    })
    sessionStorage.removeItem('selectedCust');
    if (!localStorage.getItem("sessionInfo")) {
        window.location.replace('index.html');
    }
    var sI = localStorage.getItem("sessionInfo");
    var sessionInfo = JSON.parse(sI);
    var head = sessionInfo.basicAuthenticate;
    <!-----------------------------------------start of Cust list and selection------------>
    var obj = {};
    obj['typeId'] = sessionInfo.typeId;
    obj['userId'] = sessionInfo.userId;
    obj['userType'] = sessionInfo.userType;
    var head = sessionInfo.basicAuthenticate;
    var data = JSON.stringify(obj);
    $.ajax({
        url: beUrl() + '/getcustomerlist',
        type: 'POST',
        dataType: 'json',
        processData: 'fasle',
        contentType: 'application/json',
        headers: {
            "basicauthenticate": "" + head
        },
        data: data,
        success: function(response) {
            var res = JSON.stringify(response);
            var temp = JSON.parse(res);
            var custDetail = temp.Data;
            $.each(custDetail, function(i, v) {
                var custName = v.custName;
                var custId = v.custId;
                var printex = ' <option  value="' + v.custId + '" >' + v.custName + '</option>';
                $('#custlist').append(printex);
                $('select').material_select();
            });

        }
    });
    $('select').on('change', function() {
        sessionStorage.setItem('selectedCust', '0');
        var val = this.value;
        if (val > 0) {
            sessionStorage.setItem('selectedCust', val);
        }
    });
    <!-----------------------------------------End of Cust list and selection------------>
    showbills();
});

function openmodal() {
    $("#modal1").modal('open');
}

function generateBill() {
    var val = sessionStorage.getItem('selectedCust');
    val2 = parseInt(val);
    if (val2 == 0 || !sessionStorage.getItem('selectedCust')) {
        Materialize.toast('Please Select a Coustomer', 4000);
    } else {
        var si = localStorage.getItem("sessionInfo");
        var sessionInfo = JSON.parse(si);
        var myObj = {};
        myObj['typeId'] = sessionInfo.typeId;
        myObj['userId'] = sessionInfo.userId;
        myObj['userType'] = sessionInfo.userType;
        myObj['custId'] = val;
        myObj['billName'] = "";
        var head = sessionInfo.basicAuthenticate;
        var jsonObj = JSON.stringify(myObj);
        $.ajax({
            url: beUrl() + '/generatebill.',
            type: 'POST',
            dataType: 'json',
            processData: false,
            contentType: 'application/json',
            headers: {
                "basicauthenticate": "" + head
            },
            data: jsonObj,
            success: function(response) {
                var res = JSON.stringify(response);
                var response = JSON.parse(res);
                var totalBillValue = response.totalBillValue;
                Materialize.toast('Bill Generated for &#x20b9; ' + totalBillValue + '', 4000);
                $("#modal1").modal('close');
                setTimeout(function() {
                    location.reload();
                }, 2000);
            },
            error: function(response) {
                var responseJson = JSON.stringify(response);
                var responseJsonR = JSON.parse(responseJson);
                var statusCode = responseJsonR.responseJSON;
                var data = JSON.stringify(statusCode);
                var xData = JSON.parse(data);
                var message = xData.Data;
                Materialize.toast(message, 4000);
            }
        });
    }
}

function showbills() {
    var si = localStorage.getItem("sessionInfo");
    var sessionInfo = JSON.parse(si);
    var myObj = {};
    myObj['typeId'] = sessionInfo.typeId;
    myObj['userId'] = sessionInfo.userId;
    myObj['userType'] = sessionInfo.userType;
    var head = sessionInfo.basicAuthenticate;
    var jsonObj = JSON.stringify(myObj);
    $.ajax({
        url: beUrl() + '/viewbilllist.',
        type: 'POST',
        dataType: 'json',
        processData: false,
        contentType: 'application/json',
        headers: {
            "basicauthenticate": "" + head
        },
        data: jsonObj,
        success: function(response) {
            var res = JSON.stringify(response);
            var response = JSON.parse(res);
            var totalCount = response.totalCount;

            if (totalCount == 0) {
                var printx = "You don't have any bills Right Now";

            } else {
                var orderId;
                var data = response.Data;
                var start0 = 0;
                var start1 = 0;
                var start2 = 0;
                $.each(data, function(i, v) {
                    var shopName = v.shopName;
                    var totalBill = v.totalBill;
                    var billId = v.billId;
                    var isPaid = v.isPaid;
                    var custName = v.custName;
                    var orderCount = v.orderCount;
                    var totalBill = v.totalBill;
                    var orders = v.orders;
                    if(isPaid==0){
                      start0 = 1;
                    }
                    else if(isPaid==1){
                      start1 = 1;
                    }
                    else if(isPaid==2){
                      start2 = 1;
                    }
                    var printx = "<li><p class='markpaid' style='float:right'><a class='btn red' style='margin:1em;' onclick='paybill(" + billId + ")' >Mark Paid</a></p><div class='collapsible-header shopCard_name' style='font-weight:600; font-size:1em;'> " + custName + " <br> Bill Id:" + billId + " <br> Order Count : " + orderCount + " <br> Total Bill Amount " + totalBill + " <a style='float:right'href='#' class=''>Deatils</a></div><div class='collapsible-body' id='billId" + billId + "'></li>";
                    $("#stat" + isPaid).append(printx);
                    $.each(orders, function(i, v) {
                        orderId = v.orderId;
                        var orderStatus = v.orderStatus;
                        var totalItemCount = v.totalItemCount;
                        var items = v.items;
                        var totalAmount = v.totalAmount;
                        var orderDate = v.orderDate;
                        var printy = "<div class='z-depth-3' style='padding-top:1em; padding-bottom:1em;' id='orderId" + orderId + "'> orderId " + orderId + " <br>orderStatus " + orderStatus + " <br>totalItemCount " + totalItemCount + " <br>totalAmount " + totalAmount + " <br>orderDate " + orderDate + "<div id='#orderId" + orderId + "'></div></div>";
                        $('#billId' + billId).append(printy);
                        $.each(items, function(i, v) {
                            var printz = "<br>" + v.itemName;
                            $('#orderId' + orderId).append(printz);
                        });
                    });
                    if (isPaid == 2) {
                        $('.markpaid').hide();
                    }

                });
                if(start0 == 0){
                  var lPrint = "<div class=row><div class=col s12 m12><div class=card blue-grey darken-1><div class=card-content white-text><span class=card-title>No new bills</span><p>Kindly generate some bills.</p></div></div></div></div>";
                  $("#stat0").append(lPrint);
                }
                if(start1 == 0){
                  var lPrint = "<div class=row><div class=col s12 m12><div class=card blue-grey darken-1><div class=card-content white-text><span class=card-title>No bills for verification</span><p>No new payment recieved</p></div></div></div></div>";
                  $("#stat1").append(lPrint);
                }
                if(start2 == 0){
                  var lPrint = "<div class=row><div class=col s12 m12><div class=card blue-grey darken-1><div class=card-content white-text><span class=card-title>No bills verified</span><p>No payment by user yet verified.</p></div></div></div></div>";
                  $("#stat2").append(lPrint);
                }
            }
        },
        error: function(response) {
            var responseJson = JSON.stringify(response);
            var responseJsonR = JSON.parse(responseJson);
            var statusCode = responseJsonR.responseJSON;
            var data = JSON.stringify(statusCode);
            var xData = JSON.parse(data);
            var message = xData.Data;
            Materialize.toast(message, 4000);
        }
    });
}

function paybill(billId) {
    var si = localStorage.getItem("sessionInfo");
    var sessionInfo = JSON.parse(si);
    var myObj = {};
    myObj['typeId'] = sessionInfo.typeId;
    myObj['userId'] = sessionInfo.userId;
    myObj['userType'] = sessionInfo.userType;
    myObj['billId'] = billId;
    var head = sessionInfo.basicAuthenticate;
    var jsonObj = JSON.stringify(myObj);
    $.ajax({
        url: beUrl() + '/paythebill.',
        type: 'POST',
        dataType: 'json',
        processData: false,
        contentType: 'application/json',
        headers: {
            "basicauthenticate": "" + head
        },
        data: jsonObj,
        success: function(response) {
            var res = JSON.stringify(response);
            var response = JSON.parse(res);
            var message = response.Data;
            //location.reload();
        },
        error: function(response) {
            var responseJson = JSON.stringify(response);
            var responseJsonR = JSON.parse(responseJson);
            var statusCode = responseJsonR.responseJSON;
            var data = JSON.stringify(statusCode);
            var xData = JSON.parse(data);
            var message = xData.Data;
            Materialize.toast('Bill already Paid', 4000);
        }
    });
}
