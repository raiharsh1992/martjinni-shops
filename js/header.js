$(document).ready(function(){
  $(".button-collapse").sideNav({
    closeOnClick: true,
      draggable: true
  });
  if(localStorage.getItem("sessionInfo")){
    var sessionInfo = JSON.parse(localStorage.getItem("sessionInfo"));
    if(sessionInfo.userType!="SHP"){
      localStorage.clear();
      window.location.reload();
    }
  }

  if(window.localStorage){
    if(localStorage.getItem("sessionInfo")){
      var log = '<li><a href=orders.html style="color:#4c4c4c; font-weight:450;" >Orders</a></li><li><a href=items.html style="color:#4c4c4c; font-weight:450;" >Items</a></li><li style="color:#4c4c4c; font-weight:450;" ><a href=delmed.html style="color:#4c4c4c; font-weight:450;">Delivery Boys</a></li><li style="color:#4c4c4c; font-weight:450;" ><a href=restore.html style="color:#4c4c4c; font-weight:450;">Change Password</a></li><li style="color:#4c4c4c; font-weight:450;" ><a href=logout.html style="color:#4c4c4c; font-weight:450;">Logout</a></li>';
      sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
      var myObj = {};
      myObj['userId']=sessionInfo.userId;
      myObj['typeId']=sessionInfo.typeId;
      myObj['userType']=sessionInfo.userType;
      var jsonObj = JSON.stringify(myObj);
      var head = sessionInfo.basicAuthenticate;
      var xPrint = "";
      $.ajax({
        url : beUrl()+'/getshopstatus',
        type: 'POST',
        dataType:'json',
        processData:false,
        contentType: 'application/json',
        headers : {"basicauthenticate":""+head},
        data:jsonObj,
        success: function(response) {
          var sessionInfo1 = JSON.stringify(response);
          var sessionInfo = JSON.parse(sessionInfo1);
          localStorage.setItem('storeStatus',sessionInfo.storeStatus)
          if((localStorage.getItem('storeStatus'))==0){
            xPrint = "<li><a><div class=switch><label> Closed <input unchecked  type=checkbox onclick=storestatusUpdate()><span class=lever></span></label></div></a></li>";
          }
          else if((localStorage.getItem('storeStatus'))==1){
            xPrint = "<li><a><div class=switch><label> Open <input checked  type=checkbox onclick=storestatusUpdate()><span class=lever></span></label></div></a></li>";
          }
        }
      });
      var xMyObj = {};
      xMyObj['shopId']=sessionInfo.typeId;
      var jsoData = JSON.stringify(xMyObj);
      $.ajax({
        url : beUrl()+'/getshopproperty',
        type: 'POST',
        dataType:'json',
        processData:false,
        contentType: 'application/json',
        data:jsoData,
        success: function(response) {
          var propData = JSON.parse(JSON.stringify(response)).data;
          $.each(propData, function(i,v){
            if(v.propertyName=="subscription"){
              if(v.propertyValue!=0){
                xPrint = xPrint+""+"<li><a href=subscription.html style='color:#4c4c4c; font-weight:450;'>Subscription</a></li>";
              }
            }
            if(v.propertyName=="useAddOn"){
              if(v.propertyValue!=0){
                xPrint = xPrint+""+"<li><a href=addon.html style='color:#4c4c4c; font-weight:450;'>Addon</a></li>";
              }
            }
            if(v.propertyName=="maxPromoCount" && v.propertyValue!=0){
              xPrint = xPrint+""+"<li><a href=promo.html style='color:#4c4c4c; font-weight:450;' >Promocode</a></li>"
            }
            if(v.propertyName=="useGst" && v.propertyValue!=0)
            {
              localStorage.setItem("useGst","YES");
            }
            if(v.propertyName=="useSubQuantity" && v.propertyValue!=0)
            {
              localStorage.setItem("propertyVal", v.propertyValue);
            }

          });
        }
      });
      setTimeout(function() {
        log = xPrint+""+log;
        $('#test').append(log);
        $('#test2').append(log);
      }, 900);
    }
    else{
      window.location.replace(feUrl()+"/index.html");
    }
  }
});
$('head').append("<!-- Global site tag (gtag.js) - Google Analytics --><script async src='https://www.googletagmanager.com/gtag/js?id=UA-114684493-1'></script><script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-114684493-1');</script>");
function storestatusUpdate() {
  if((localStorage.getItem('sessionInfo'))&&(localStorage.getItem('storeStatus'))){
    var sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
    var storeStatus = localStorage.getItem('storeStatus');
    var myObj = {};
    var custStatus;
    if(storeStatus==1){
      custStatus = 0;
    }
    else{
      custStatus =1;
    }
    myObj['userId'] = sessionInfo.userId;
    myObj['typeId'] = sessionInfo.typeId;
    myObj['userType'] = sessionInfo.userType;
    myObj['storeStatus'] = custStatus;
    var jsonObj = JSON.stringify(myObj);
    var head = sessionInfo.basicAuthenticate;
    $.ajax({
      url : beUrl()+'/updateshopstatus',
      type: 'POST',
      dataType:'json',
      processData:false,
      contentType: 'application/json',
      headers : {"basicauthenticate":""+head},
      data:jsonObj,
      success: function(response) {
        location.reload();
        },
      error: function (response) {
        var responseJson = JSON.stringify(response);
        var responseJsonR = JSON.parse(responseJson);
        var statusCode = responseJsonR.responseJSON;
        var data = JSON.stringify(statusCode);
        var xData = JSON.parse(data);
        var message = xData.Data;
        Materialize.toast(message, 4000);
        console.log(message);
      }
    });
  }
  else{
    window.location.replace(feUrl()+"/index.html");
  }
}
