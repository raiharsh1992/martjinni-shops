importScripts('https://www.gstatic.com/firebasejs/4.1.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/4.1.1/firebase-messaging.js');
importScripts('https://www.gstatic.com/firebasejs/4.1.1/firebase.js');


var config = {
apiKey: "AIzaSyDVvGpg-qatublDZeflhaCZ--RNj5qKFdk",
authDomain: "usermartjinni.firebaseapp.com",
databaseURL: "https://usermartjinni.firebaseio.com",
projectId: "usermartjinni",
storageBucket: "usermartjinni.appspot.com",
messagingSenderId: "899688528611"
};

firebase.initializeApp(config);
const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function(payload) {
    const notificationTitle = payload.data.title;
    const notificationOptions = {
        body: payload.data.body
    };

return self.registration.showNotification(notificationTitle,
    notificationOptions);
});
